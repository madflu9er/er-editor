const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const resolve = require('resolve');

const mode = process.env.NODE_ENV || 'development';
const prod = mode === 'development';

module.exports = {
	mode: 'development',
	entry: {
		bundle: './src/main.js'
	},
	resolve: {
		modules: [path.resolve(__dirname, 'src'), 'node_modules', 'components'],
		alias: {
			components: path.resolve(__dirname, './src/components'),
			connector: path.resolve(__dirname, 'src/components/connector'),
			editor: path.resolve(__dirname, 'src/components/editor'),
			entity: path.resolve(__dirname, 'src/components/entity'),
			field: path.resolve(__dirname, 'src/components/field'),
			line: path.resolve(__dirname, 'src/components/line'),
			marker: path.resolve(__dirname, 'src/components/marker'),
			point: path.resolve(__dirname, 'src/components/point'),
			relation: path.resolve(__dirname, 'src/components/relation'),
		},
		extensions: ['.mjs', '.js', '.svelte', '.ts'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	devtool: "#inline-source-map",
	output: {
		path: __dirname + '/public',
		filename: '[name].js',
		chunkFilename: '[name].[id].js'
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				loader: 'awesome-typescript-loader',
				exclude: /node_modules/,
				options: {
					"useCache": true
				}
			},
			{
				test: /\.svelte$/,
				use: {
					loader: 'svelte-loader',
					options: {
						emitCss: true,
						hotReload: true
					}
				}
			},
			{
				test: /\.css$/,
				use: [
					/**
					 * MiniCssExtractPlugin doesn't support HMR.
					 * For developing, use 'style-loader' instead.
					 * */
					prod ? MiniCssExtractPlugin.loader : 'style-loader',
					'css-loader'
				]
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css'
		})
	],
};
