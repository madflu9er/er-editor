// eslint-disable-next-line max-classes-per-file
import { writable, Readable, get } from "svelte/store";

/** Callback to inform of a value updates. */
declare type Subscriber<T> = (value: T) => void;
/** Cleanup logic callback. */
declare type Invalidator<T> = (value?: T) => void;
/** Unsubscribes from value updates. */
declare type Unsubscriber = () => void;

type SubscribeFunc<T> = (run: Subscriber<T>, invalidate?: Invalidator<T>) => Unsubscriber;

export class CReadStore<T> implements Readable<T> {
    public subscribe: SubscribeFunc<T>;
    constructor(subscribe: SubscribeFunc<T>) {
        this.subscribe = subscribe;
    }

    public get value(): T {
        return get(this);
    }
}

type SetFunc<T> = (value: T) => void;
type UpdateFunc<T> = (updater: (value: T) => T) => void;

export class CStore<T> {
    public store: CReadStore<T>;
    public set: SetFunc<T>;
    public update: UpdateFunc<T>;

    constructor(set: SetFunc<T>, update: UpdateFunc<T>, store: CReadStore<T>) {
        this.set = set;
        this.update = update;
        this.store = store;
    }

    public get value(): T {
        return get(this.store);
    }
}

export function cStore<T>(value: T): CStore<T> {
    const res = writable<T>(value);
    const { set, update, subscribe } = res;
    const store = new CReadStore<T>(subscribe);

    return new CStore<T>(set, update, store);
}
