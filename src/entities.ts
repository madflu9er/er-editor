import { IEntity, FieldModificators } from "interfaces";

export const entities: IEntity[] = [
    {
        caption: "Candidate",
        entityCd: "candidate",
        fields: [{
                caption: "Candidate ID",
                columnCd: "candidate_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 20
            },
            {
                caption: "Offer",
                columnCd: "offer",
                modificator: FieldModificators.foreignKey,
                type: "string",
                valueLength: 40
            },
            {
                caption: "Name",
                columnCd: "name",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 15
            },
            {
                caption: "Surname",
                columnCd: "surname",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 15
            }
        ]
    },
    {
        caption: "Vacancy",
        entityCd: "vacancy",
        fields: [
            {
                caption: "Vacancy ID",
                columnCd: "vacancy_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Candidate",
                columnCd: "candidate",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Description",
                columnCd: "description",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 400
            },
        ]
    },
    {
        caption: "Vacancy History",
        entityCd: "vacancy_history",
        fields: [
            {
                caption: "Vacancy",
                columnCd: "vacancy",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Project",
                columnCd: "project",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 40
            },
            {
                caption: "Salary",
                columnCd: "salary",
                modificator: FieldModificators.field,
                type: "number",
                valueLength: 10
            },
        ]
    },
    {
        caption: "Vacancy Status",
        entityCd: "vacancy_status",
        fields: [
            {
                caption: "Status ID",
                columnCd: "status_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Vacancy",
                columnCd: "vacancy",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 10
            },
            {
                caption: "Type",
                columnCd: "type",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 10
            },
        ]
    },
    {
        caption: "Vacancy Tag",
        entityCd: "vacancy_tag",
        fields: [
            {
                caption: "Tag ID",
                columnCd: "tag_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Vacancy",
                columnCd: "vacancy",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 10
            },
            {
                caption: "Type",
                columnCd: "type",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 10
            },
        ]
    },
    {
        caption: "Candidate History",
        entityCd: "candidate_history",
        fields: [
            {
                caption: "Candidate",
                columnCd: "candidate",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Offer",
                columnCd: "offer",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 10
            },
            {
                caption: "Vacancy",
                columnCd: "vacancy",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 10
            },
            {
                caption: "Description",
                columnCd: "description",
                modificator: FieldModificators.field,
                type: "string",
                valueLength: 400
            },
        ]
    },
    {
        caption: "Employee",
        entityCd: "employee",
        fields: [
            {
                caption: "Employee ID",
                columnCd: "employee_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Department",
                columnCd: "department",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Position",
                columnCd: "position",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            }
        ]
    },
    {
        caption: "Department",
        entityCd: "department",
        fields: [
            {
                caption: "Department ID",
                columnCd: "department_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Employee",
                columnCd: "employee",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Position",
                columnCd: "position",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            }
        ]
    },
    {
        caption: "Position",
        entityCd: "position",
        fields: [
            {
                caption: "Position ID",
                columnCd: "position_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Department",
                columnCd: "department",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Vacancy",
                columnCd: "vacancy",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            }
        ]
    },
    {
        caption: "Project",
        entityCd: "project",
        fields: [
            {
                caption: "Project ID",
                columnCd: "project_id",
                modificator: FieldModificators.primartyKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Employee",
                columnCd: "employee",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            },
            {
                caption: "Department",
                columnCd: "department",
                modificator: FieldModificators.foreignKey,
                type: "number",
                valueLength: 15
            }
        ]
    },
];
