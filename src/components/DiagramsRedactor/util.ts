export const getParrentWithType = (element, value: string) => {
    if (!element.getAttribute)
        return null;
    const res = element.getAttribute("type");
    if (res === value)
        return element
    else
        return getParrentWithType(element.parentNode, value);
}

export const getRect = (event, editor) => {
    const editorBox = editor.getBoundingClientRect();
    const mousePos = {
        x: event.clientX - editorBox.x,
        y: event.clientY - editorBox.y
    };

    return {x: mousePos.x, y: mousePos.y, height: 100, width: 100}
};