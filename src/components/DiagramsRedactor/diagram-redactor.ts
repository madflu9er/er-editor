import { IDiagram, IDiagramEntity } from "interfaces";

export class DiagramRedactor {
    public diagrams: IDiagram[];
    public entities: IDiagramEntity[];
    public currentDiagram: IDiagram;

    constructor(entities: IDiagramEntity[], diagrams: IDiagram[]) {
        this.currentDiagram = diagrams && diagrams.length ? diagrams[0] : null;
        this.entities = entities;
        this.diagrams = diagrams;
    }

    public addNewDiagram(d: IDiagram) {
        this.diagrams.push(d);
    }

    public changeCurrentDiagram(d: IDiagram) {
        this.currentDiagram = d;
    };
}

