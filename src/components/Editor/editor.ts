import { IArea, RelationPosition, IRelation, IDiagram, IEntity, IDiagramEntity, FieldModificators } from "interfaces";
import { EREntity } from "components/Entity/er-entity";
import { Relation } from "components/Relation/relation";

export class Editor {
    public name: string;
    public area: IArea;
    private entitiesMap: Map<string, EREntity>;
    private relationsMap: Map<string, Relation>

    private defWidth = 900;
    private defHeight = 400;
    public border = 4;

    constructor(diagram: IDiagram, entities: IEntity[]) {
        this.name = diagram.name;
        this.area = diagram.area;
        this.setEntitiesMap(diagram, entities);
        this.setRelationsMap(diagram, entities);
    }

    private setEntitiesMap(diagram: IDiagram, entities: IEntity[]) {
        this.entitiesMap = new Map<string, EREntity>();
        const arr: IDiagramEntity[] = diagram?.entities?.length
            ? diagram.entities
            : generateDiagramEntities(entities, this.area);

        arr.forEach(_ => this.entitiesMap.set(_.entity.entityCd, new EREntity(_)));
    }

    private setRelationsMap(diagram: IDiagram, entities: IEntity[]) {
        this.relationsMap = new Map<string, Relation>();
        const arr = diagram?.relations?.length
            ? diagram.relations
            : generateDiagramRelations(entities);

        arr.forEach((r) => {
            const from = this.entitiesMap.get(r.fromEntity);
            const to = this.entitiesMap.get(r.toEntity);
            if(!from || !to)
                return;
            const key = from.entityCd + to.entityCd;
            this.relationsMap.set(key, new Relation(from, to, r));
        })
    }

    public get width() {
        return this.area?.width || this.defWidth;
    }

    public get height() {
        return this.area?.height || this.defHeight;
    }

    public get entities(): EREntity[] {
        const entities = [];
        this.entitiesMap.forEach((e) => {
            entities.push(e);
        });
        return entities;
    }

    public get relations(): Relation[] {
        const relations: Relation[] = [];
        this.relationsMap.forEach((e) => {
            relations.push(e);
        });
        return relations;
    }

    public getEntityByCd(entityCd: string): EREntity {
        return this.entitiesMap.get(entityCd);
    }

    public isValidRelation(fromEntityCd: string, toEntityCd: string) {
        if (fromEntityCd === toEntityCd)
            return;

        const key = fromEntityCd + toEntityCd;
        const reverseKey = toEntityCd + fromEntityCd;

        return !this.relationsMap.get(key) && !this.relationsMap.get(reverseKey);
    }

    public addRelation(fromEntityCd: string, toEntityCd: string, fromPosition: RelationPosition, toPosition: RelationPosition) {
        if (!this.isValidRelation(fromEntityCd, toEntityCd))
            return;

        const fromEntity = this.getEntityByCd(fromEntityCd);
        const toEntity = this.getEntityByCd(toEntityCd);
        const key = fromEntityCd + toEntityCd;
        const relation: IRelation = {
            fromEntity: fromEntity?.entityCd,
            fromEntityPosition: fromPosition,
            fromEntityShift: 0,
            toEntity: toEntity?.entityCd,
            toEntityPosition: toPosition,
            toEntityShift: 0,
            points: []
        };

        this.relationsMap.set(key, new Relation(fromEntity, toEntity, relation));
    }

    public addEntity(entity: IDiagramEntity) {
        this.entitiesMap.set(entity.entity.entityCd, new EREntity(entity));
        return this;
    }

    public getNotUsedEntities(entities: IEntity[]) {
        return entities.filter(_ => !this.entities.find(l => l.entityCd === _.entityCd));
    };
}

const generateDiagramEntities = (entities: IEntity[], area: IArea): IDiagramEntity[] => {
    const { width, height } = area;
    const defEntityWidth = 100;
    const defEntityHeight = 100;
    const colsNum = Math.floor(Math.sqrt(entities.length));
    const rowsNum = Math.ceil(entities.length / colsNum);
    const leftMargin = Math.floor((width - colsNum * defEntityWidth) / (colsNum + 1));
    const topMargin = Math.floor((height - rowsNum * defEntityHeight) / (rowsNum + 1));
    const diagramEntities: IDiagramEntity[] = [];
    let counter = 0;

    for (let i = 0; i < rowsNum; i++) {
        for (let j = 0; j < colsNum; j++) {
            if (!entities[counter])
                break;

            diagramEntities.push({
                rect: {
                    x: leftMargin + (leftMargin + defEntityWidth) * j,
                    y: topMargin + (topMargin + defEntityHeight) * i,
                    height: defEntityHeight,
                    width: defEntityWidth
                },
                entity: entities[counter]
            })
            counter++;
        }
    }
    return diagramEntities;
};

interface IGenRelation {
    from: IEntity;
    to: IEntity;
}

const generateDiagramRelations = (entities: IEntity[]): IRelation[] => {
    const relationsMap = new Map<string, IGenRelation>();
    const relations = [];

    entities.forEach(e => {
        e.fields.filter(field => field.modificator === FieldModificators.foreignKey)
            .forEach(field => {
                const key = e.entityCd + field.columnCd;
                const to = entities.find(e => e.entityCd === field.columnCd);
                if (to && !relationsMap.get(key))
                    relationsMap.set(key, { from: e, to});
            })
    });

    relationsMap.forEach(({ from, to }) => {
        relations.push({
            fromEntity: from.entityCd,
            fromEntityPosition: RelationPosition.left,
            fromEntityShift: 0,
            toEntity: to.entityCd,
            toEntityPosition: RelationPosition.right,
            toEntityShift: 0,
            points: []
        })
    })

    return relations;
};

const getAttFromParent = (event: MouseEvent, attribute: string): string => {
    let elem = event.target as HTMLElement;
    const grid = event.currentTarget;
    while (elem && elem !== grid) {
        const rowId = elem.getAttribute(attribute);
        if (rowId != null)
            return rowId;
        elem = elem.parentElement;
    }
};

export const getEntityCdFromEvent = (event: MouseEvent): string =>
    getAttFromParent(event, "entityCd");
