import { IRect } from "interfaces";

export const getNewRect = (e: MouseEvent, axis: string, rect: IRect, node, editor) => {
    const nodeBox = node.getBoundingClientRect();
    const editorBox = editor.getBoundingClientRect();

    switch(axis) {
        case "y": {
            rect.height = Math.max(100, rect.height - (e.clientY - rect.y));

            if (rect.height > 100)
                rect.y = e.clientY;
        }; break;

        case "height": {
            rect.height = Math.max(100, rect.height + (e.clientY - rect.height - nodeBox.y));
        }; break;

        case "x": {
            rect.width = Math.max(100, rect.width - (e.clientX - nodeBox.x));

            if (rect.width > 100)
                rect.x = e.clientX - editorBox.x;
        }; break;

        case "width": {
            rect.width = Math.max(100, rect.width + (e.clientX - rect.width - rect.x - editorBox.x));
        }; break;
    }

    return rect;
}