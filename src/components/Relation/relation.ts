import { CReadStore } from "main/store";
import { IPoint, RelationPosition, IRelation, IRect } from "interfaces";
import { EREntity } from "components/Entity/er-entity";

interface IEntityPoint extends IPoint {
    position: RelationPosition;
    entityName?: string;
}

interface ILine {
    x1: number;
    x2: number;
    y1: number;
    y2: number;
}

export class Relation implements IRelation {
    public fromEntity: string;
    public toEntity: string;
    public fromRect: CReadStore<IRect>;
    public toRect: CReadStore<IRect>;
    public fromEntityPosition: RelationPosition;
    public fromEntityShift: number;
    public toEntityPosition: RelationPosition;
    public toEntityShift: number;
    public points: IPoint[];

    public startPoint: IEntityPoint;
    public endPoint: IEntityPoint;

    public markerWidth: number = 20;

    constructor(fromEntity: EREntity, toEntity: EREntity, relation: IRelation) {
        this.fromEntity = fromEntity.entityCd;
        this.toEntity = toEntity.entityCd;
        this.fromEntityPosition = relation.fromEntityPosition;
        this.toEntityPosition = relation.toEntityPosition;
        this.fromEntityShift = relation.fromEntityShift;
        this.toEntityShift = relation.toEntityShift;
        this.points = relation.points;

        this.fromRect = fromEntity.rect;
        this.toRect = toEntity.rect;
    }

    private getEntityPoint(coords: IRect, position: RelationPosition): IEntityPoint {
        const { width, x, height, y } = coords;

        switch(position) {
            case RelationPosition.bottom :
                return { x: x + (width / 2), y: y + height + (this.markerWidth) + 4, position}
            case RelationPosition.top :
                return { x: x + (width / 2), y: y - this.markerWidth, position}
            case RelationPosition.left :
                return { x: x - this.markerWidth , y: coords.y + (height / 2), position}
            case RelationPosition.right :
                return { x: x + (width) + 4 + this.markerWidth, y: y + (height / 2), position}
        }
    }

    public getLines(fromRect: IRect, toRect: IRect): ILine[] {
        this.startPoint = this.getEntityPoint(fromRect, this.fromEntityPosition);
        this.endPoint = this.getEntityPoint(toRect, this.toEntityPosition);

        return [{
            x1: this.startPoint.x,
            y1: this.startPoint.y,
            x2: this.endPoint.x,
            y2: this.endPoint.y
        }];
    }
}
