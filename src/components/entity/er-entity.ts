import { CStore, CReadStore, cStore } from "main/store";
import { IRect, IDiagramEntity, RelationPosition, IField, FieldModificators } from "interfaces";

interface ICursorOffSet {
    x: number;
    y: number;
}

export class EREntity {
    private _rect: CStore<IRect>;
    public rect: CReadStore<IRect>

    public entityCd: string;
    public caption: string;
    public fields: IField[];
    public collapsed: boolean;

    constructor(diagramEntity: IDiagramEntity) {
        this.entityCd = diagramEntity.entity.entityCd;
        this.caption = diagramEntity.entity.caption;
        this._rect = cStore(diagramEntity.rect);
        this.rect = this._rect.store;
        this.fields = diagramEntity.entity.fields;
        this.collapsed = false;
    }

    public changeCaption(caption: string) {
        this.caption = caption;
        return this;
    }

    public changeFieldCaption(fieldCaption: string) {
        const field = this.fields.find(f => f.caption === fieldCaption);

        if (field)
            field.caption = name;

        return this;
    }

    public updateRect(newRect: IRect) {
        this._rect.update(_ => newRect);
    }

    public addField(type: string, caption: string, valueLength = 0, modificator = FieldModificators.field) {
        this.fields.push({
            caption,
            columnCd: caption,
            type,
            modificator,
            valueLength: valueLength ? valueLength : 100
        });
        return this;
    }

    private getNewX(newEntityX: number, entityBox: DOMRect, editorBox: DOMRect): number {
        if (newEntityX < 0)
            return 0;

        if (newEntityX + entityBox.width > editorBox.width)
            return editorBox.width - entityBox.width;

        return newEntityX;
    }

    private getNewY(newEntityY: number, entityBox: DOMRect, editorBox: DOMRect): number {
        if (newEntityY < 0)
            return 0;

        if (newEntityY + entityBox.height > editorBox.height)
            return editorBox.height - entityBox.height;

        return newEntityY;
    }

    public onEntityMove(e: MouseEvent, editor: HTMLElement, entity: HTMLElement, cursorOffSet: ICursorOffSet) {
        const editorBox = editor.getBoundingClientRect();
        const entityBox = entity.getBoundingClientRect();

        const newEntityX = e.clientX - cursorOffSet.x - editorBox.x;
        const newEntityY = e.clientY - cursorOffSet.y - editorBox.y;

        this._rect.update(r => {
            r.x = this.getNewX(newEntityX, entityBox, editorBox);
            r.y = this.getNewY(newEntityY, entityBox, editorBox);
            return r;
        });
    }

    public getPoint(position: RelationPosition, horizontalShift = 0, verticalShift = 0, ) {
        const { x, y, width, height } = this.rect.value;

        switch(position) {
            case RelationPosition.bottom :
                return { x: x + horizontalShift, y: y + height - verticalShift, position, entityCd: this.entityCd}
            case RelationPosition.top :
                return { x: x + horizontalShift, y: y - verticalShift , position, entityCd: this.entityCd}
            case RelationPosition.left :
                return { x: x - horizontalShift, y: y + verticalShift, position, entityCd: this.entityCd}
            case RelationPosition.right :
                return { x: x + width - horizontalShift, y: y + verticalShift, position, entityCd: this.entityCd}
        }
    }

    public getPoints(markerHeight: number, border: number) {
        const coords = this.rect.value;

        return [
            this.getPoint(RelationPosition.top, (coords.width - (markerHeight)) / 2, (markerHeight + border) / 2),
            this.getPoint(RelationPosition.bottom, (coords.width - (markerHeight)) / 2, (markerHeight) / 2),
            this.getPoint(RelationPosition.left, (markerHeight + border) / 2, (coords.height - markerHeight) / 2),
            this.getPoint(RelationPosition.right, border / 2, (coords.height - markerHeight) / 2)
        ];
    }

    public setWidth(width: number) {
        this._rect.update(_ => {
            _.width = width;
            return _;
        })
    }

    public setHeight(height: number) {
        this._rect.update(_ => {
            _.height = height;
            return _;
        })
    }
}
