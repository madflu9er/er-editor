import { IDiagram, FieldModificators, RelationPosition } from "interfaces";

export const diagram: IDiagram = {
    name:"Vacancy Relations",
    area: { width: 1280, height: 800 },
    entities: [
        {
            rect: {
                x: 530, y: 80, height: 140, width: 132
            },
            entity: {
                caption: "Candidate",
                entityCd: "candidate",
                fields: [{
                        caption: "Candidate ID",
                        columnCd: "candidate_id",
                        modificator: FieldModificators.primartyKey,
                        type: "number",
                        valueLength: 20
                    },
                    {
                        caption: "Offer ID",
                        columnCd: "offer_id",
                        modificator: FieldModificators.foreignKey,
                        type: "string",
                        valueLength: 40
                    },
                    {
                        caption: "Name",
                        columnCd: "name",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 15
                    },
                    {
                        caption: "Surname",
                        columnCd: "surname",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 15
                    }
                ]
            }
        },
        {
            rect: {
                x: 180, y: 150, height: 140, width: 132
            },
            entity: {
                caption: "Vacancy",
                entityCd: "vacancy",
                fields: [
                    {
                        caption: "Vacancy ID",
                        columnCd: "vacancy_id",
                        modificator: FieldModificators.primartyKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Candidate ID",
                        columnCd: "candidate_id",
                        modificator: FieldModificators.foreignKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Description",
                        columnCd: "description",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 400
                    },
                ]
            }
        },
        {
            rect: {
                x: 345, y: 480, height: 140, width: 155
            },
            entity: {
                caption: "Vacancy History",
                entityCd: "vacancy_history",
                fields: [
                    {
                        caption: "Vacancy ID",
                        columnCd: "vacancy_id",
                        modificator: FieldModificators.foreignKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Project",
                        columnCd: "project",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 40
                    },
                    {
                        caption: "Salary",
                        columnCd: "salary",
                        modificator: FieldModificators.field,
                        type: "number",
                        valueLength: 10
                    },
                ]
            }
        },
    ],
    relations: [
        {
            fromEntity: "candidate",
            fromEntityPosition: RelationPosition.left,
            fromEntityShift: 0,
            toEntity: "vacancy",
            toEntityPosition: RelationPosition.right,
            toEntityShift: 0,
            points: []
        },
        {
            fromEntity: "vacancy",
            fromEntityPosition: RelationPosition.bottom,
            fromEntityShift: 0,
            toEntity: "vacancy_history",
            toEntityPosition: RelationPosition.top,
            toEntityShift: 0,
            points: []
        }
    ]
};

export const diagram1: IDiagram = {
    name:"Vacancy Relations",
    area: { width: 580, height: 700 },
    entities: [
        {
            rect: {
                x: 530, y: 80, height: 140, width: 132
            },
            entity: {
                caption: "Candidate",
                entityCd: "candidate",
                fields: [{
                        caption: "Candidate ID",
                        columnCd: "candidate_id",
                        modificator: FieldModificators.primartyKey,
                        type: "number",
                        valueLength: 20
                    },
                    {
                        caption: "Offer ID",
                        columnCd: "offer_id",
                        modificator: FieldModificators.foreignKey,
                        type: "string",
                        valueLength: 40
                    },
                    {
                        caption: "Name",
                        columnCd: "name",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 15
                    },
                    {
                        caption: "Surname",
                        columnCd: "surname",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 15
                    }
                ]
            }
        },
        {
            rect: {
                x: 180, y: 150, height: 140, width: 132
            },
            entity: {
                caption: "Vacancy",
                entityCd: "vacancy",
                fields: [
                    {
                        caption: "Vacancy ID",
                        columnCd: "vacancy_id",
                        modificator: FieldModificators.primartyKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Candidate ID",
                        columnCd: "candidate_id",
                        modificator: FieldModificators.foreignKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Description",
                        columnCd: "description",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 400
                    },
                ]
            }
        },
        {
            rect: {
                x: 345, y: 480, height: 140, width: 155
            },
            entity: {
                caption: "Vacancy History",
                entityCd: "vacancy_history",
                fields: [
                    {
                        caption: "Vacancy ID",
                        columnCd: "vacancy_id",
                        modificator: FieldModificators.foreignKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Project",
                        columnCd: "project",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 40
                    },
                    {
                        caption: "Salary",
                        columnCd: "salary",
                        modificator: FieldModificators.field,
                        type: "number",
                        valueLength: 10
                    },
                ]
            }
        },
    ],
    relations: [
        {
            fromEntity: "vacancy",
            fromEntityPosition: RelationPosition.bottom,
            fromEntityShift: 0,
            toEntity: "vacancy_history",
            toEntityPosition: RelationPosition.top,
            toEntityShift: 0,
            points: []
        }
    ]
};

export const diagram2: IDiagram = {
    name:"Diagram 2",
    area: { width: 580, height: 700 },
    entities: [],
    relations: []
};

export const diagram3: IDiagram = {
    name:"Diagram 3",
    area: { width: 580, height: 700 },
    entities: [
        {
            rect: {
                x: 345, y: 480, height: 140, width: 155
            },
            entity: {
                caption: "Vacancy History",
                entityCd: "vacancy_history",
                fields: [
                    {
                        caption: "Vacancy ID",
                        columnCd: "vacancy_id",
                        modificator: FieldModificators.foreignKey,
                        type: "number",
                        valueLength: 15
                    },
                    {
                        caption: "Project",
                        columnCd: "project",
                        modificator: FieldModificators.field,
                        type: "string",
                        valueLength: 40
                    },
                    {
                        caption: "Salary",
                        columnCd: "salary",
                        modificator: FieldModificators.field,
                        type: "number",
                        valueLength: 10
                    },
                ]
            }
        },
    ],
    relations: []
};
