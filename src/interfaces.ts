export enum FieldModificators {
    primartyKey = "PK",
    foreignKey = "FK",
    field = ""
}

export interface IArea {
    width: number;
    height: number;
}

export interface IField {
    caption: string;
    columnCd: string;
    modificator: FieldModificators;
    type: string;
    valueLength: number;
}

export interface IEntity {
    caption: string;
    entityCd: string;
    fields: IField[];
}

export interface IDiagramEntity {
    rect: IRect;
    entity: IEntity;
}

export interface IRelation {
    fromEntity: string,
    fromEntityPosition: RelationPosition,
    fromEntityShift: number,
    toEntity: string,
    toEntityPosition: RelationPosition,
    toEntityShift: number,
    points: IPoint[];
}

export interface IDiagram {
    name: string;
    area: IArea;
    entities: IDiagramEntity[];
    relations: IRelation[];
}

export interface IRect {
    x: number;
    y: number;
    height: number;
    width: number;
}

export enum RelationPosition {
    right = "R",
    left = "L",
    top = "T",
    bottom = "B"
}

export interface IPoint {
    x: number;
    y: number;
}
